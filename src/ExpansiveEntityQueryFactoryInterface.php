<?php


namespace Drupal\expansive_entity_query;


interface ExpansiveEntityQueryFactoryInterface {

  /**
   * @param string $entity_type
   * @param string $conjunction
   *
   * @return \Drupal\expansive_entity_query\ExpansiveEntityQuery\QueryInterface
   */
  public function get($entity_type, $conjunction = 'AND');
}
