<?php


namespace Drupal\expansive_entity_query\ExpansiveEntityQuery;

use Drupal\expansive_entity_query\ExpansiveEntityQueryException;

/**
 * An entity query but with possibility to add custom calculated fields
 *
 * @package Drupal\expansive_entity_query
 */
class Query extends \Drupal\Core\Entity\Query\Sql\Query implements QueryInterface {

  /** @var array $expressionFields | Expression fields */
  protected $expressionFields;

  /** @var array $extendedFieldList | Extended fields combined */
  protected $extendedFieldList;

  /**
   * Builds and adds all extended fields to the SQL query
   *
   * @return $this
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\expansive_entity_query\ExpansiveEntityQueryException
   */
  protected function addExtendedFields() {

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $field_plugin_factory */
    $field_plugin_factory = \Drupal::service('plugin.manager.expansive_entity_query.field');

    foreach($this->extendedFieldList as $alias => $field) {
      if (empty($field['type'])) {
        throw new ExpansiveEntityQueryException('Field type empty');
      }

      /** @var array $sqlFields */
      $sqlFields = [];
      foreach($field['data']['fields'] as $inputField) {
        if (empty($inputField['field'])) {
          throw new ExpansiveEntityQueryException('Field data missing');
        }
        $inputFieldName = $inputField['field'];
        $inputFieldLangcode = $inputField['langcode'] ?? NULL;
        $sqlFields[$inputFieldName] = $this->getSqlField($inputFieldName, $inputFieldLangcode);
      }

      /** @var \Drupal\expansive_entity_query\Plugin\FieldInterface $field_plugin */
      $field_plugin = $field_plugin_factory->createInstance($field['type']);

      /** @var string $expression */
      $expression = $field_plugin->provideExpression($field['data'], $sqlFields);

      $this->expressionFields[$alias] = [
        'type' => $field['type'],
        'data' => $field['data'],
        'expression' => $expression,
      ];
      $this->sqlQuery->addExpression($expression, $alias);
    }
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function extendedField($alias, $type, $data) {
    if (isset($this->extendedFieldList[$alias])) {
      throw new ExpansiveEntityQueryException('Field "' . $alias . '" already exists');
    }

    if (!isset($data['fields'])) {
      $data['fields'] = [];
    }

    $this->extendedFieldList[$alias] = [
      'type' => $type,
      'data' => $data,
    ];

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function getSqlField($field, $langcode) {
    if (!empty($this->expressionFields[$field])) {
      return $field;
    }
    else {
      return parent::getSqlField($field, $langcode);
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function addSort() {
    if ($this->count) {
      $this->sort = [];
    }
    // Gather the SQL field aliases first to make sure every field table
    // necessary is added. This might change whether the query is simple or
    // not. See below for more on simple queries.
    $sort = [];
    if ($this->sort) {
      foreach ($this->sort as $key => $data) {
        $sort[$key] = $this->getSqlField($data['field'], $data['langcode']);
      }
    }
    $simple_query = $this->isSimpleQuery();
    // If the query is set up for paging either via pager or by range or a
    // count is requested, then the correct amount of rows returned is
    // important. If the entity has a data table or multiple value fields are
    // involved then each revision might appear in several rows and this needs
    // a significantly more complex query.
    if (!$simple_query) {
      // First, GROUP BY revision id (if it has been added) and entity id.
      // Now each group contains a single revision of an entity.
      foreach ($this->sqlFields as $field) {
        $group_by = "$field[0].$field[1]";
        $this->sqlGroupBy[$group_by] = $group_by;
      }
      // Then, we have to add expression fields to the GROUP BY list
      foreach ($this->expressionFields as $sqlField => $expressionField) {
        $this->sqlGroupBy[$sqlField] = $sqlField;
      }
    }
    // Now we know whether this is a simple query or not, actually do the
    // sorting.
    foreach ($sort as $key => $sql_alias) {
      $direction = $this->sort[$key]['direction'];
      if ($simple_query || isset($this->sqlGroupBy[$sql_alias]) || isset($this->expressionFields[$sql_alias])) {
        // Simple queries, the grouped columns of complicated queries
        // and expression fields can be ordered normally, without the aggregation function.
        $this->sqlQuery->orderBy($sql_alias, $direction);
        // We need also to check if the field exists in the expressions list
        if (!isset($this->sqlFields[$sql_alias]) && !isset($this->expressionFields[$sql_alias])) {
          $this->sqlFields[$sql_alias] = explode('.', $sql_alias);
        }
      }
      else {
        // Order based on the smallest element of each group if the
        // direction is ascending, or on the largest element of each group
        // if the direction is descending.
        $function = $direction == 'ASC' ? 'min' : 'max';
        $expression = "$function($sql_alias)";
        $expression_alias = $this->sqlQuery->addExpression($expression);
        $this->sqlQuery->orderBy($expression_alias, $direction);
      }
    }
    return $this;
  }

  /**
   * @return array|int
   * @throws \Drupal\Core\Entity\Query\QueryException
   * @throws \Drupal\expansive_entity_query\ExpansiveEntityQueryException
   */
  public function execute() {
    // Everything the same as in the parent class, but with addExtendedFields
    return $this
      ->prepare()
      ->addExtendedFields()
      ->compile()
      ->addSort()
      ->finish()
      ->result();
  }
}
