<?php

namespace Drupal\expansive_entity_query\ExpansiveEntityQuery;

use Drupal\Core\Entity\Query\QueryFactoryInterface;

/**
 * ExpansiveQuery factory.
 *
 * @package Drupal\expansive_entity_query
 */
class QueryFactory extends \Drupal\Core\Entity\Query\Sql\QueryFactory implements QueryFactoryInterface{

}
