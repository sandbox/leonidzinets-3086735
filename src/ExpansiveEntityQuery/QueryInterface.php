<?php


namespace Drupal\expansive_entity_query\ExpansiveEntityQuery;


interface QueryInterface extends \Drupal\Core\Entity\Query\QueryInterface {

  /**
   * Adds an extended field to the query
   *
   * @param string $alias
   *   Field alias
   * @param string $type
   *   Field type
   * @param array $data
   *   Field data
   *
   * @return \Drupal\expansive_entity_query\ExpansiveEntityQuery\QueryInterface
   * @throws \Drupal\expansive_entity_query\ExpansiveEntityQueryException
   */
  public function extendedField($alias, $type, $data);
}
