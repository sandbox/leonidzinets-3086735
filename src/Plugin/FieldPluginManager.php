<?php


namespace Drupal\expansive_entity_query\Plugin;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Provides ExpansiveEntityQuery field plugin manager
 *
 * @package Drupal\expansive_entity_query\Plugin
 */
class FieldPluginManager extends DefaultPluginManager {
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ExpansiveEntityQuery',
      $namespaces,
      $module_handler,
      'Drupal\expansive_entity_query\Plugin\FieldInterface',
      'Drupal\Component\Annotation\Plugin',
      ['Drupal\expansive_entity_query\Annotation']
    );

    $this->alterInfo('expansive_entity_query_field_info');
    $this->setCacheBackend($cache_backend, 'expansive_entity_query_field_info_plugins');
  }
}
