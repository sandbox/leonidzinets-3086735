<?php


namespace Drupal\expansive_entity_query\Plugin;


interface FieldInterface {

  /**
   * Returns the needed expression according to the input data
   *
   * @param array $data
   *   A data array from the extendedField call
   * @param array $fields
   *   An associative array of SQL fields used in the query
   * @return string
   *   The MySQL expression
   */
  public function provideExpression($data, $fields);
}
