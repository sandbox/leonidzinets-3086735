<?php


namespace Drupal\expansive_entity_query;


class ExpansiveEntityQueryFactory implements ExpansiveEntityQueryFactoryInterface {

  /**
   * The ExpansiveEntityQuery/QueryFactory to product the actual queries
   *
   * @var \Drupal\Core\Entity\Query\QueryFactoryInterface $sqlQueryFactory
   */
  protected $sqlQueryFactory;

  /**
   * Entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * ExpansiveEntityQueryFactory constructor.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactoryInterface $sqlQueryFactory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct($sqlQueryFactory, $entityTypeManager) {
    $this->sqlQueryFactory = $sqlQueryFactory;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public function get($entity_type, $conjunction = 'AND') {
    return $this->sqlQueryFactory->get($this->entityTypeManager->getDefinition($entity_type), $conjunction);
  }
}
