<?php


namespace Drupal\expansive_entity_query\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines Expansive Entity Query field type
 *
 * @package Drupal\expansive_entity_query\Annotation
 *
 * @Annotation
 */
class ExpansiveEntityQueryField extends Plugin {

  /**
   * The plugin ID, used as $type in the extendedField method
   * @var string $id
   */
  public $id;

  /**
   * RESERVED: field category. Default value will be "expression"
   * @var string optional $category
   */
  public $category;
}
