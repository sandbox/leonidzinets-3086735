<?php


namespace Drupal\mymodule\Plugin\ExpansiveEntityQuery\Field;

use Drupal\expansive_entity_query\Plugin\FieldInterface;
use Drupal\expansive_entity_query\ExpansiveEntityQueryException;

/**
 * Plugin implementation of the Sperical Distance query field
 *
 * @ExpansiveEntityQueryField(
 *   id = "spherical_distance"
 * )
 */
class SphericalDistance implements FieldInterface{

  /**
   * {@inheritDoc}
   */
  public function provideExpression($data, $fields) {
    $fieldName = reset($fields);
    if (empty($fieldName)) {
      throw new ExpansiveEntityQueryException('MySQL field name missing');
    }

    if (!isset($data['longitude'])) {
      throw new ExpansiveEntityQueryException('Longitude parameter missing');
    }

    if (!isset($data['latitude'])) {
      throw new ExpansiveEntityQueryException('Latitude parameter missing');
    }

    $longitude = $data['longitude'];
    $latitude = $data['latitude'];

    $expression = 'ST_Distance_Sphere(Point(' .
      $longitude .
      ', ' .
      $latitude .
      '), ST_PointFromText(' .
      $fieldName .
      '))';

    return $expression;
  }
}
